\contentsline {section}{\numberline {0.1}Abstract}{2}%
\contentsline {chapter}{List of Figures}{ii}%
\contentsline {chapter}{\numberline {1}Introduction}{1}%
\contentsline {section}{\numberline {1.1}Group Data Sharing over Cloud}{1}%
\contentsline {section}{\numberline {1.2}Standard Security Techniques}{2}%
\contentsline {section}{\numberline {1.3}Our Apporach - Use of Key Agreement Protocol}{2}%
\contentsline {section}{\numberline {1.4}Problem Statement}{4}%
\contentsline {section}{\numberline {1.5}Objectives}{5}%
\contentsline {chapter}{\numberline {2}Literature Survey}{6}%
\contentsline {section}{\numberline {2.1}An efficient data security system for group data sharing in cloud system environment \cite {rpaper1}}{6}%
\contentsline {section}{\numberline {2.2}Mona: Secure Multi-owner Data Sharing for Dynamic Groups in the Cloud \cite {rpaper2}}{7}%
\contentsline {section}{\numberline {2.3}Group Key Management Protocol for File Sharing on Cloud Storage \cite {rpaper3}}{9}%
\contentsline {section}{\numberline {2.4}pKAS: A Secure Password-Based Key Agreement Scheme for the Edge Cloud \cite {rpaper4}}{10}%
\contentsline {section}{\numberline {2.5}A Privacy-Preserving and Untraceable Group Data Sharing Scheme in Cloud Computing \cite {rpaper5} }{11}%
\contentsline {chapter}{\numberline {3}System Requirement Specification}{13}%
\contentsline {section}{\numberline {3.1}Introduction}{13}%
\contentsline {subsection}{\numberline {3.1.1}Purpose}{13}%
\contentsline {subsection}{\numberline {3.1.2}Intended Audience}{13}%
\contentsline {section}{\numberline {3.2}Overall Description}{13}%
\contentsline {subsection}{\numberline {3.2.1}Operations :}{13}%
\contentsline {section}{\numberline {3.3}User characteristics}{14}%
\contentsline {subsection}{\numberline {3.3.1}Limitation}{14}%
\contentsline {subsection}{\numberline {3.3.2}Assumption and Dependencies}{14}%
\contentsline {section}{\numberline {3.4}External Interface Requirements}{14}%
\contentsline {subsection}{\numberline {3.4.1}Software Interface Requirements}{14}%
\contentsline {subsection}{\numberline {3.4.2}Hardware Interface Requirements}{15}%
\contentsline {subsection}{\numberline {3.4.3}Communication Interface Requirements}{15}%
\contentsline {section}{\numberline {3.5}Functional Requirements}{15}%
\contentsline {section}{\numberline {3.6}Non-Functional Requirements}{15}%
\contentsline {subsection}{\numberline {3.6.1}Performance Requirements}{15}%
\contentsline {subsection}{\numberline {3.6.2}Security Requirements}{16}%
\contentsline {chapter}{\numberline {4}System Design}{17}%
\contentsline {section}{\numberline {4.1}Flow Diagram}{17}%
\contentsline {chapter}{\numberline {5}Module Architecture}{19}%
\contentsline {chapter}{\numberline {6}Stage Plan}{22}%
